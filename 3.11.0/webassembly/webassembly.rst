.. index::
   ! Webassembly

.. _python_webassembly:

=====================================================
**Webassembly**
=====================================================


Python doc
==========

- https://docs.python.org/3.11/whatsnew/3.11.html

CPython now has experimental support for cross compiling to WebAssembly
platform wasm32-emscripten.
The effort is inspired by previous work like Pyodide. (Contributed by
Christian Heimes and Ethan Smith in `bpo-40280 <https://github.com/python/cpython/issues/84461>`_.)


Henry Schreiner III
========================

- https://iscinumpy.dev/post/python-311/

This is the first version of CPython to directly support WebAssembly
(wasm32-emscripten)!

You can use Python 3.10 today through Pyodide, but 3.11 should directly
support it, making it easier on Pyodide, as well as enabling other
distributions for web browsers.

Native support should mean good performance and light weight download
sizes, too!
