
.. _python_3_11_0_speed:

=====================================================
**Python 3.11.0** speed
=====================================================

- https://github.com/faster-cpython/ideas/blob/main/FasterCPythonDark.pdf


Le créateur de Python veut rendre son langage deux fois plus rapide
======================================================================

- https://www.zdnet.com/article/python-programming-we-want-to-make-the-language-twice-as-fast-says-its-creator/
- https://www.zdnet.fr/actualites/le-createur-de-python-veut-rendre-son-langage-deux-fois-plus-rapide-39922729.htm


"Nous sommes loin d'être certains d'atteindre une vitesse 2x supérieure !",
écrit-il. "Mais nous sommes optimistes et curieux".

Il a néanmoins évoqué l'idée d'une vitesse multipliée par cinq après
Python 3.11, en précisant que "nous devrons faire preuve de créativité".

Selon lui, les principaux bénéficiaires des changements à venir dans
Python seront ceux qui exécutent du "code Python pur à forte intensité
de CPU" et les utilisateurs de sites Web avec Python intégré.

Il n'y aura cependant pas beaucoup d'avantages pour le code déjà écrit
en C, comme NumPy et TensorFlow, le code lié aux E/S, le code multithreading
et le code algorithmiquement inefficace.
