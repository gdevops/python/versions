
.. index::
   pair: Atomic ; Grouping

.. _atomic_grouping:

==============================================================
**Regular expressions atomic grouping**
==============================================================

- https://www.python.org/downloads/release/python-3110b4/
- https://github.com/python/cpython/issues/34627/

Description
=============

Atomic grouping ((?>...)) and possessive quantifiers (\*+, ++, ?+, {m,n}+)
are now supported in regular expressions.
