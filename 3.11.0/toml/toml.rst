.. index::
   pair: TOML ; 3.11


.. _python_3_11_toml:

=====================================================
**Python 3.11 TOML (Tom’s Obvious Minimal Language)**
=====================================================

RealPython article
====================

- https://realpython.com/python-toml/


Introduction
--------------

Use TOML as a Configuration Format

TOML is short for Tom’s Obvious Minimal Language and is humbly named
after its creator, Tom Preston-Werner.

It was designed expressly to be a configuration file format that should
be “easy to parse into data structures in a wide variety of languages” (Source).

In this section, you’ll start thinking about configuration files and look
at what TOML brings to the table.
