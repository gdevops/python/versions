
.. _python_3_11_0_rc1:

=====================================================
**Python 3.11.0rc1 (2022-08-08)**
=====================================================

- https://docs.python.org/3.11/whatsnew/3.11.html
- https://x.com/pyblogsal/status/1556687421928083457?s=20&t=sNJlgHBu3b6uznVKomPtaA
- https://www.python.org/downloads/release/python-3110b1/
- https://discuss.python.org/t/python-3-11-1rc1-is-now-available/18068


discuss
========

- https://discuss.python.org/t/python-3-11-1rc1-is-now-available/18068

This release, 3.11.0rc1, is the penultimate release preview.
Entering the release candidate phase, only reviewed code changes which
are clear bug fixes are allowed between this release candidate and the
final release.

The second candidate and the last planned release preview is currently
planned for Monday, 2022-09-05 while the official release is planned
for Monday, 2022-10-03.

There will be no ABI changes from this point forward in the 3.11 series
and the goal is that there will be as few code changes as possible.


twitter
=======

- https://x.com/pyblogsal/status/1555575914871902210?s=20&t=sNJlgHBu3b6uznVKomPtaA
- https://x.com/pyblogsal/status/1556687421928083457?s=20&t=sNJlgHBu3b6uznVKomPtaA

Python 3.11.0 is almost ready 🎉💪 The last release, Python 3.11 release
candidate 1 (3.11.0rc1), is the penultimate release preview before we
get to the official release in October 👀🗓️

Please, help us to test it 🐞🐛😎

You can download it here:

webassembly
================

- https://discuss.python.org/t/python-3-11-1rc1-is-now-available/18068/3

Unofficial WebAssembly builds are available at https://github.com/tiran/cpython-wasm-test/releases/tag/v3.11.0rc1 .

I have uploaded an interactive REPL to my GitHub page, too. It uses a
hack to work around missing HTTP headers and may not work in every browser.
