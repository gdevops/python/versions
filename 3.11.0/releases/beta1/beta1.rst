
.. _python_3_11_0_beta1:

=====================================================
**Python 3.11.0beta1 (2022-05-08)**
=====================================================

- https://docs.python.org/3.11/whatsnew/3.11.html
- https://www.python.org/downloads/release/python-3110b1/
- https://discuss.python.org/t/python-3-11-0b1-is-now-available/15602


discuss
========

We did it, team! :partying_face: After quite a bumpy release process and
a bunch of last-time fixes, we have reached beta 1 and feature freeze.

What a ride eh? :rocket: You can get the shiny new release artefacts
from `here <https://www.python.org/downloads/release/python-3110b1/>`_

twitter
=======

- https://x.com/pyblogsal/status/1523635910696652800?s=20&t=VYLPoBhyx_ou50zU_Cr1mg


Python 3.11.0 beta 1 is here! 🎉🐍 This marks feature freeze which means
that no new features or APIs will be added to 3.11 and only bugfixes 🐞
are now allowed.

Please, if you maintain any python package help us to test that everything
works as expected🙏
