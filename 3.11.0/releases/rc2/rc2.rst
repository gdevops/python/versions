
.. _python_3_11_0_rc2:

=====================================================
**Python 3.11.0rc2 (2022-09-12)**
=====================================================

- https://docs.python.org/3.11/whatsnew/3.11.html
- https://www.python.org/downloads/release/python-3110rc2/
- https://discuss.python.org/t/python-3-11-0rc2-is-now-available/18988


discuss
========

- https://discuss.python.org/t/python-3-11-0rc2-is-now-available/18988

Python 3.11 is one month away, can you believe it? This snake is still
trying to bite as it has been an interesting day of fighting fires, release
blockers, and a bunch of late bugs but your friendly release team always
delivers :slight_smile:

This is the second release candidate of Python 3.11

This release, 3.11.0rc2, is the last preview before the final release
of Python 3.11.0 on 2022-10-24.

Entering the release candidate phase, only reviewed code changes which
are clear bug fixes are allowed between this release candidate and the
final release.

The second candidate and the last planned release preview is currently
planned for Monday, 2022-09-05 while the official release is planned
for Monday, 2022-10-24.

There will be no ABI changes from this point forward in the 3.11 series
and the goal is that there will be as few code changes as possible.
Modification of the final release

Due to the fact that we needed to delay the last release candidate by a
week and because of personal scheduling problems I am delaying the final
release to 2022-10-24 (three weeks from the original date).



twitter
=======

- https://x.com/pyblogsal/status/1569324965752619009?s=20&t=hQLv6Di76lTzNTrxCu4K5w

Python 3.11.0🐍is one month 📅away, can you believe it? Meanwhile, we
have the final release candidate for you while we squash the last bugs 🐞,
polish the docs 📕and make sure the final release will be as shiny as
possible✨ Get the latest RC here: https://www.python.org/downloads/release/python-3110rc2/
