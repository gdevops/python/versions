.. index::
   pair: Releases ; 3.11.0


.. _python_3_11_0_plan:

================================
Python 3.11 Release Schedule
================================

- https://www.python.org/dev/peps/pep-0664/ (Python 3.11 Release Schedule)

Actual
========

- 3.11 development begins: Monday, 2021-05-03
- 3.11.0 alpha 1: Monday, 2021-10-05
- 3.11.0 alpha 2: Tuesday, 2021-11-02
- 3.11.0 alpha 3: Wednesday, 2021-12-08
- 3.11.0 alpha 4: Monday, 2022-01-14
- 3.11.0 alpha 5: Thursday, 2022-02-03
- 3.11.0 alpha 6: Monday, 2022-03-07
- 3.11.0 alpha 7: Tuesday, 2022-04-05
- 3.11.0 beta 1: Monday, 2022-05-06
- 3.11.0 beta 2: Monday, 2022-05-30
- 3.11.0 beta 3: Thursday, 2022-06-16
- 3.11.0 beta 4: Saturday, 2022-07-09
- 3.11.0 candidate 1: Monday, 2022-08-01
- 3.11.0 candidate 2: Monday, 2022-09-12

Expected
==========

- 3.11.0 final: Monday, 2022-10-24

Subsequent bugfix releases every two months.

Pre-releases
==============

.. toctree::
   :maxdepth: 3

   rc2/rc2
   rc1/rc1
   beta1/beta1
