

.. _python_3_11_0_beta5:

=====================================================
Python 3.11.0 **beta5, (2022-07-26)**
=====================================================


Python 3.11.0b5
================

- https://www.python.org/downloads/release/python-3110b5/


Release Date: July 26, 2022
This is a beta preview of Python 3.11

Python 3.11 is still in development. 3.11.0b5 is the last of the five
planned beta release previews.

Beta release previews are intended to give the wider community the
opportunity to test new features and bug fixes and to prepare their
projects to support the new feature release.

We strongly encourage maintainers of third-party Python projects to test
with 3.11 during the beta phase and report issues found to the Python
bug tracker as soon as possible.

While the release is planned to be feature complete entering the beta phase,
it is possible that features may be modified or, in rare cases, deleted
up until the start of the release candidate phase (Monday, 2021-08-02).

Our goal is have no ABI changes after beta 4 and as few code changes as
possible after 3.11.0rc1, the first release candidate.
To achieve that, it will be extremely important to get as much exposure
for 3.11 as possible during the beta phase.

Please keep in mind that this is a preview release and its use is not recommended for production environments.


Beta5 by https://x.com/pyblogsal
----------------------------------------

- https://x.com/pyblogsal/status/1551880046457094145?s=20&t=SojWMsjNqgnA98aLDyoQ6Q


Here we are. The universe. The vastness of spacetime. At the edge.
The last frontier. The last beta \*(conditions apply) for Python 3.11 🐍.

We have defied the gods of release blockers and we have won by using the
required amount of ruse and subterfuge.


.. figure:: images/beta5_2022_07_26.png
   :align: center

   https://x.com/pyblogsal/status/1551880046457094145?s=20&t=SojWMsjNqgnA98aLDyoQ6Q
