.. index::
   ! Fine Grained Error Locations

.. _fine_grained_error_locations:

====================================================================
**PEP 0657 Include Fine Grained Error Locations in Tracebacks**
====================================================================

- https://www.python.org/dev/peps/pep-0657/
- https://bugs.python.org/issue43950

Authors
========

- Pablo Galindo <pablogsal at python.org>,
- Batuhan Taskaya <batuhan at python.org>,
- Ammar Askar <ammar at ammaraskar.com>



.. figure:: pep_657.png
   :align: center

.. figure:: get_instructions.png
   :align: center

.. figure:: point_2.png
   :align: center

.. figure:: horrendous.png
   :align: center
