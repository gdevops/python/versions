.. index::
   ! dataklass

.. _dataclass_transforms:

====================================================================
**Data klass transforms**
====================================================================

- https://docs.python.org/3.11/whatsnew/3.11.html
- https://peps.python.org/pep-0681/
- https://www.slideshare.net/AnthonyShaw5/whats-new-in-python-311
- https://github.com/dabeaz/dataklasses


.. _dataclass_transforms_example_1:

Data class transforms example 1
=================================


.. code-block:: python

    from dataklass import dataklass

    @dataklass
    class Car:
        make: str
        model: str
        engine_capacity: float
        turbo: bool =False


    mycar = Car("Audi", "RS4", "2.5", True)
    mycar.engine_capacity = 1.2


.. figure:: pep_681.png
   :align: center
