
.. index::
   pair: Asyncio ; Task Groups
   ! Tasks groups

.. _asyncio_tasks_groups:

==============================================================
**Asyncio Tasks Groups**
==============================================================

- https://www.slideshare.net/AnthonyShaw5/whats-new-in-python-311

Description
==============

- replacement for gather() API
- Exceptions arre raised as an Exception Group.


.. _asyncio_tasks_groups_example_1:

Asyncio Tasks Groups example 1
================================


.. figure:: images/tasks_group.png
   :align: center

   https://www.slideshare.net/AnthonyShaw5/whats-new-in-python-311

.. code-block:: python

    import asynio

    async def connect(server: str, port: 1234):
        ...

    async def main():
        try:
            async with asyncio.TaskGroup() as tg:
                tg.create_task(connect("localhost", 1234))
                tg.create_task(connect("localhost", 4356))
        except* OSError as eg:
            # handle exception group
