
.. index::
   pair: Exception ; Notes
   ! Exception notes

.. _exception_notes:

==============================================================
**Exception Notes**
==============================================================

- https://peps.python.org/pep-0678/ (Enriching Exceptions with Notes)
- https://towardsdatascience.com/top-3-new-features-in-python-3-11-prepare-yourself-701cca4af9c3
- https://www.slideshare.net/AnthonyShaw5/whats-new-in-python-311

Description
=============

Python’s Exception class will have a __note__ attribute in Python 3.11.

It's None by default, but you can override it with any string you want.
Sure, this isn't the most groundbreaking feature, but a note here and
there can come in handy if you have dozens of custom exception classes.

Here’s the code we’ll run in both containers

.. code-block:: python


    class CustomException(Exception):
        __note__ = "Note about my custom exception"


    if __name__ == "__main__":
        raise CustomException()


.. _exception_notes_example_1:

Exception Notes example 1
==========================

- https://www.slideshare.net/AnthonyShaw5/whats-new-in-python-311

.. figure:: images/exception_notes.png
   :align: center

   https://www.slideshare.net/AnthonyShaw5/whats-new-in-python-311


.. code-block:: python

    def process(order):
        if len(order) > 2
            raise ValueError("Too many items on order")
        elif len(order) < 2:
            raise ValueError("Not enough items in order")

    orders = [
        ["eggs", "spam"],
        ["ham"],
        ["spinach", "ricotta", "salad"],
        ["cola", "rice"],
    ]

    errors = []
    for order in orders:
        try:
            process.order(order)
        exception Exception as ex:
            ex.add_note(", ".join(order)
            errors.append(ex)

    if errors:
        raise ExceptionGroup("Order issues", errors)
