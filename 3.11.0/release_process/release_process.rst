
.. figure:: team.png
   :align: center


.. _releasing_python_3_11_0:

=====================================================
**Releasing Python 3.11 (2022-10-22)**
=====================================================

- https://www.youtube.com/watch?v=PGZPSWZSkJI


.. figure:: py_release.png
   :align: center

Other links
============

Episode 130: Fostering an Internal Python Community & Managing the 3.11 Release
----------------------------------------------------------------------------------

- https://realpython.com/podcasts/rpp/130/?linkId=186819829


Pablo describes how the Python Guild started and currently operates inside
Bloomberg. We talk about how it fosters community and acts as a way to
promote internally developed tools across disparate teams.

We also discuss how work groups use it to find new internal candidates
for their teams.

Pablo talks about his role as release manager for Python 3.10 and 3.11.

He shares the intense journey the team has had this year in preparing
for the release of 3.11. He details updating testing strategies to work
with the new specializing adaptive interpreter.


Check-in the release process
==============================

.. figure:: logo_python_3_11.png
   :align: center


Python 3.11 is released
==============================

.. figure:: python_3_11_is_released.png
   :align: center
