.. index::
   pair: Typing; Self

.. _typing_0675:

====================================================================
**PEP 0675 – Arbitrary Literal String Type**
====================================================================

- https://docs.python.org/3.11/whatsnew/3.11.html
- https://peps.python.org/pep-0673/
