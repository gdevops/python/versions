.. index::
   pair: Typing; Self

.. _self_typing:

====================================================================
**PEP 0673 Self**
====================================================================

- https://docs.python.org/3.11/whatsnew/3.11.html
- https://peps.python.org/pep-0673/
