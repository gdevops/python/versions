
.. _dictitems_missing:

=====================================================================================
**PEP 655 – Marking individual TypedDict items as required or potentially-missing**
=====================================================================================

- https://docs.python.org/3.11/whatsnew/3.11.html
- https://peps.python.org/pep-0655/
