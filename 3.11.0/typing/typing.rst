.. index::
   pair: Typing ; 3.11


.. _python_3_11_typing:

=====================================================
**Python 3.11 typing**
=====================================================


.. toctree::
   :maxdepth: 3

   arbitray_literal_string_type/arbitray_literal_string_type
   dict_items_required/dict_items_required
   self/self
   variadic_generics/variadic_generics
