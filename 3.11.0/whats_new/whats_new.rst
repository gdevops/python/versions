
.. _whats_new_3_11:

=====================================================
**Whats new**
=====================================================


Python documentation
========================

- https://docs.python.org/3.11/whatsnew/3.11.html


RealPython
============

- https://realpython.com/python311-new-features/
