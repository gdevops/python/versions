
.. index::
   pair: Exception ; Groups
   ! Exception groups

.. _exception_groups:

==============================================================
**PEP 654 -- Exception Groups** and except\* (Issue 45292)
==============================================================

- https://x.com/IritKatriel
- https://www.python.org/dev/peps/pep-0654/
- https://bugs.python.org/issue45292

Authors
==========

- Irit Katriel <iritkatriel at gmail.com>, https://x.com/IritKatriel
- Yury Selivanov <yury at edgedb.com>,
- Guido van Rossum <guido at python.org>


Issue 45292
=============

- https://bugs.python.org/issue45292


Created on 2021-09-26 12:07 by iritkatriel, last changed 2022-01-06 19:06
by iritkatriel.


Videos
=======

- https://x.com/IritKatriel/status/1578684970867953668?s=20&t=IbFgWpuya5M81Jrxgxd_kg

Python 3.11 introduces new features that make it possible to raise and
handle multiple unrelated exceptions.

My talk at PyConUK explains how these new features work and goes over
some of the design decisions we made.

- https://www.youtube.com/watch?v=uARIj9eAZcQ (Exception Groups and except: Irit Katriel)




Examples
=========

- :ref:`exception_notes_example_1`
- :ref:`asyncio_tasks_groups_example_1`
