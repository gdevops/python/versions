
.. index::
   pair: Python ; 3.13.0 (future)


.. _current_python_release:
.. _last_python_dev_release:
.. _python_3_13_0:

=====================================================
**Python 3.13.0** (DEV)
=====================================================

- https://docs.python.org/3.13/whatsnew/3.13.html


Summary – Release Highlights
=================================

Python 3.13 beta is the pre-release of the next version of the Python 
programming language, with a mix of changes to the language, the implementation 
and the standard library. 

The biggest changes to the implementation include:

- a new interactive interpreter, 
- and experimental support for dropping the Global Interpreter Lock (PEP 703) 
- and a Just-In-Time compiler (`PEP 744 <https://peps.python.org/pep-0744/>`_). 

The library changes contain removal of deprecated APIs and modules, as 
well as the usual improvements in user-friendliness and correctness.

Interpreter improvements
=============================

- A greatly improved interactive interpreter and improved error messages.
- Color support in the new interactive interpreter, as well as in tracebacks 
  and doctest output. 
  This can be disabled through the PYTHON_COLORS and NO_COLOR environment variables.
- PEP 744: `A basic JIT compiler <https://docs.python.org/3.13/whatsnew/3.13.html#whatsnew313-jit-compiler>`_ was added. 
  It is currently disabled by default (though we may turn it on later). 
  Performance improvements are modest – we expect to be improving this 
  over the next few releases.
- PEP 667: The locals() builtin now has defined semantics when mutating 
  the returned mapping. 
  Python debuggers and similar tools may now more reliably update local 
  variables in optimized scopes even during concurrent code execution


New typing features
========================

- `PEP 696 <https://peps.python.org/pep-0696/>`_: Type parameters (typing.TypeVar, typing.ParamSpec, and typing.TypeVarTuple) 
  **now support defaults**.
- PEP 702: Support for marking deprecations in the type system using the new warnings.deprecated() decorator.
- PEP 742: typing.TypeIs was added, providing more intuitive type narrowing behavior.
- PEP 705: typing.ReadOnly was added, to mark an item of a typing.TypedDict as read-only for type checkers.

Removed modules
========================

- PEP 594: The remaining 19 “dead batteries” have been removed from the 
  standard library: aifc, audioop, cgi, cgitb, chunk, crypt, imghdr, mailcap, 
  msilib, nis, nntplib, ossaudiodev, pipes, sndhdr, spwd, sunau, telnetlib, 
  uu and xdrlib.

- Also removed were the tkinter.tix and lib2to3 modules, and the 2to3 program.
