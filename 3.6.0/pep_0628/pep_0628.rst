
.. index::
   pair: Python ; math.Tau
   ! Tau

.. _python_tau:

=============================================
PEP-0628 Add **math.tau**
=============================================

.. seealso::

   - https://www.python.org/dev/peps/pep-0628/
   - https://tauday.com/
   - http://www.math.utah.edu/~palais/pi.html




Abstract
===========

In honour of Tau Day 2011, this PEP proposes the addition of the circle
constant math.tau to the Python standard library.

The concept of tau (τ) is based on the observation that the ratio of a
circle's circumference to its radius is far more fundamental and
interesting than the ratio between its circumference and diameter.

It is simply a matter of assigning a name to the value **2 * pi (2π)**


The Rationale for Tau
=======================

pi is defined as the ratio of a circle's circumference to its diameter.

However, a circle is defined by its centre point and its radius. This is
shown clearly when we note that the parameter of integration to go from
a circle's circumference to its area is the radius, not the diameter.

If we use the diameter instead we have to divide by four to get rid of
the extraneous multiplier.

When working with radians, it is trivial to convert any given fraction
of a circle to a value in radians in terms of tau.

A quarter circle is tau/4, a half circle is tau/2, seven 25ths is 7*tau/25,
etc.

In contrast with the equivalent expressions in terms of pi (pi/2, pi, 14*pi/25),
the unnecessary and needlessly confusing multiplication by two is gone.


::

    In [1]: import math

    In [2]: math.tau
    Out[2]: 6.283185307179586

    In [3]: math.pi
    Out[3]: 3.141592653589793



Other Resources
===================

I've barely skimmed the surface of the many examples put forward to point
out just how much easier and more sensible **many aspects of mathematics
become when conceived in terms of tau rather than pi**.

If you don't find my specific examples sufficiently persuasive, here are
some more resources that may be of interest:

- Michael Hartl is the primary instigator of Tau Day in his `Tau Manifesto <https://tauday.com/>`_
- Bob Palais, the author of the original mathematics journal article
  highlighting the problems with pi `has a page of resources <http://www.math.utah.edu/~palais/pi.html>`_ on the topic
- For those that prefer videos to written text, `Pi is wrong! <http://www.youtube.com/watch?v=IF1zcRoOVN0>`_
  and `Pi is (still) wrong <http://www.youtube.com/watch?v=jG7vhMMXagQ>`_ are available on YouTube
