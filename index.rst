
.. index::
   pair: Python ; Versions
   ! Versions

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>


|FluxWeb| `RSS <https://gdevops.frama.io/python/versions/rss.xml>`_

.. _python_versions:

=====================
**Python** versions
=====================

- https://github.com/python/cpython
- https://github.com/python/cpython/tags
- https://github.com/python/cpython/releases.atom |FluxWeb| 
- https://github.com/python/cpython/commits.atom |FluxWeb|

:last stable Python release: :ref:`current_python_release`
:last next stable Python release: :ref:`next_python_release`
:last dev Python release: :ref:`last_python_dev_release`

.. figure:: images/status_python_versions.png
   :align: center

   https://devguide.python.org/versions/#versions


.. figure:: images/resume_versions.png
   :align: center

   https://www.slideshare.net/AnthonyShaw5/whats-new-in-python-311

.. toctree::
   :maxdepth: 6

   release_peps/release_peps
   3.14.0/3.14.0
   3.13.0/3.13.0
   3.12.9/3.12.9
   3.12.4/3.12.4
   3.12.3/3.12.3
   3.12.1/3.12.1
   3.12.0/3.12.0
   3.11.7/3.11.7
   3.11.5/3.11.5
   3.11.4/3.11.4
   3.11.3/3.11.3
   3.11.2/3.11.2
   3.11.0/3.11.0
   3.10.0/3.10.0
   3.9.7/3.9.7
   3.9.6/3.9.6
   3.9.5/3.9.5
   3.9.4/3.9.4
   3.9.2/3.9.2
   3.9.0/3.9.0
   3.8/3.8
   3.7/3.7
   3.6.0/3.6.0
   3.4.0/3.4.0
