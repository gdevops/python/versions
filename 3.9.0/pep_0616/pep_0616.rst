
.. _python39_string_functions:

==============================================================================
**PEP 616** String methods to remove prefixes and suffixes
==============================================================================

.. seealso::

   - https://www.python.org/dev/peps/pep-0616/
   - https://docs.python.org/3.9/whatsnew/3.9.html#pep-616-new-removeprefix-and-removesuffix-string-methods




Articles
=========

https://martinheinz.dev
--------------------------

math module is not the only one that got some new functions. Two new convenience functions for strings were added too:

**removeprefix**
++++++++++++++++++

.. code-block:: python

    # Remove prefix
    "someText".removeprefix("some")
    # "Text"


**removesuffix**
++++++++++++++++++

.. code-block:: python

    # Remove suffix
    "someText".removesuffix("Text")
    # "some"

These 2 functions perform what you would otherwise achieve using
string[len(prefix):] for prefix and string[:-len(suffix)] for suffix.

These are very simple operations and therefore also very simple
functions, but considering that you might perform these operations
quite often, it's nice to have built-in function that does it for you.
