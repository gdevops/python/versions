
.. _python_3_9_async:

==============================================================================
Python 3.9 **async news**
==============================================================================





A number of improvements have been made to the asyncio and multiprocessing library in this release
=====================================================================================================

.. seealso::

   - https://bugs.python.org/issue30966
   - https://towardsdatascience.com/10-awesome-python-3-9-features-b8c27f5eba5c

As an instance,

- The reuse_address parameter of asyncio.loop.create_datagram_endpoint()
  is no longer supported due to significant security concerns.
- New coroutines, shutdown_default_executor() and coroutine asyncio.to_thread()
  have been added.

  The shutdown_default_executor schedules a shutdown for the default
  executor that waits on the ThreadPoolExecutor to finish closing.

  The asyncio.to_thread() is mainly used for running IO-bound functions
  in a separate thread to avoid blocking the event loop.

With regards to the multiprocessing library improvements, a new method
close() has been added to the multiprocessing.SimpleQueue class.
**This method explicitly closes the queue.**
This will ensure that the queue is closed and does not stay around for
longer than expected. The key to remember is that the methods get(), put(),
empty() must not be called once the queue is closed.




coroutine asyncio.to_thread(func, /, \*args, \*\*kwargs)
===========================================================

.. seealso::

   - https://docs.python.org/3.9/library/asyncio-task.html?highlight=to_thread#asyncio.to_thread



https://discuss.python.org/t/what-are-the-advantages-of-asyncio-over-threads/2112/26
----------------------------------------------------------------------------------------

.. seealso::

   - https://discuss.python.org/t/what-are-the-advantages-of-asyncio-over-threads/2112/26
   - https://github.com/aeros

::

    PS: In Python 3.9+, I fairly recently added asyncio.to_thread() that
    is a bit more simple to work with than loop.run_in_executor() for
    working with threads in asyncio.

https://discuss.python.org/t/ok-to-pass-loop-to-another-thread/3456/6
-----------------------------------------------------------------------

.. seealso::

   - https://discuss.python.org/t/ok-to-pass-loop-to-another-thread/3456/6


::

    asyncio.to_thread() is effectively a high-level version of loop.run_in_executor()
    that we just recently added.
    It behaves similarly to loop.run_in_executor(None, func, *args), but
    uses the current running event loop instead of calling run_in_executor()
    on any event loop instance, and directly accepts kwargs.
