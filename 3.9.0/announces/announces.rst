
.. _python_3_9_0_announces:

=======================================================
**Python 3.9.0** announces
=======================================================

.. contents:
   :depth: 3


Python 3.9 is now officially available! by Łukasz Langa 22h10 UTC+2
============================================================================

.. seealso::

   - https://x.com/llanga

.. figure:: ../announce_3_9_0.png
   :align: center

#Python 3.9 is now officially available! Get it here: https://python.org/downloads/release/python-390/

Released on time, on budget, great features and optimizations, a million
household uses! Easily our best release ever!

That is until 3.10 next year. But you go pester the new RM @pyblogsal
for THAT!

Python 3.9: Cool New Features for You to Try by Geir Arne Hjelle
==================================================================

.. toctree::
   :maxdepth: 3

   gahjelle/gahjelle


Python 3.9 par Serdar Yegulalp Serdar Yegulalp
===================================================

.. toctree::
   :maxdepth: 3

   serdar_yegulalp/sedar_yegulalp


Python 3.9: What's New by Jan Giacomelli
===========================================

.. toctree::
   :maxdepth: 3

   jan_giacomelli/jan_giacomelli.rst


Sortie de Python 3.9 par Antoine Rozo (entwanne)
===================================================

.. toctree::
   :maxdepth: 3

   antoine_rozo/antoine_rozo


10 Awesome Python 3.9 Features by Farhad Malik
==================================================

.. toctree::
   :maxdepth: 3

   farhad_malik/farhad_malik


Video top 10 new things in python3.9 (beginner-intermediate) anthony explains #093 by Anthony Sottile
==========================================================================================================

.. seealso::

   - https://www.youtube.com/watch?v=Dtw0QJhepV0
   - https://x.com/codewithanthony
   - https://x.com/codewithanthony/status/1313177904692387842?s=20

today I discuss and demo my top favorite new things in python3.9!

links:

- getting python3.9 early via deadsnakes: https://launchpad.net/~deadsnakes/+ar...
- what's new in python3.9: https://docs.python.org/3.9/whatsnew/3.9.html
- stdin / stdout / stderr video: https://www.youtube.com/watch?v=5za6e...
- decorators: https://www.youtube.com/watch?v=WDMr6...

- 0:00 - (intro) / how I have python3.9 already
- 01:18 - builtin generic types (for type annotations)
- 03:05 - dictionary merge operators
- 04:58 - str.removeprefix / str.removesuffix

sys.stderr is now line buffered everywhere
--------------------------------------------

- 06:38 - sys.stderr is now line buffered everywhere

ast.unparse function
----------------------

- 08:30 - ast.unparse function
- 10:00 - new PEG parser and secret bonus new with syntax

__file__ of interactive scripts is now an absolute path
----------------------------------------------------------

- 13:13 - __file__ of interactive scripts is now an absolute path

new functions in curses for tabsize / escdelay
------------------------------------------------

- 14:37 - new functions in curses for tabsize / escdelay

decorators can be any expression
--------------------------------------

- 15:45 - decorators can be any expression

new modules (zoneinfo, graphlib)
---------------------------------

- 17:00 - new modules (zoneinfo, graphlib)
