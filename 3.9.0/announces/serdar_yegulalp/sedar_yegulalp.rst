
.. _python_3_9_0_serdar:

===========================================================================================================================
Python 3.9 : des améliorations et des innovations par **Serdar Yegulalp**  (adaptation Jean Elyan)
===========================================================================================================================

.. seealso::

   - https://www.infoworld.com/article/3543885/python-39-whats-new-and-better.html
   - https://www.infoworld.com/author/Serdar-Yegulalp/
   - https://x.com/syegulalp
   - https://www.lemondeinformatique.fr/actualites/lire-python-39-des-ameliorations-et-des-innovations-80627.html




Introduction
==============

Cette mise à jour majeure de Python inaugure un nouveau calendrier de
sortie, plus rapide, améliore les performances, apporte des fonctions
de chaînes de caractères plus pratiques, des opérateurs d'union sur
les dictionnaires et des API de classes internes plus cohérentes et
plus stables.

La version 3.9 de Python apporte des changements significatifs à la fois
aux caractéristiques du langage et à la manière dont il est développé.

Ces dernières années, la popularité de Python n’a cessé de croître, et
son usage a explosé dans des domaines en forte évolution comme la science
des données et le machine learning. Le projet fait en sorte de suivre
le rythme de toutes les nouvelles demandes.

Voici un aperçu des nouveautés les plus importantes apportées au langage.

Adoption d’un cycle annuel de publication
==========================================

Jusqu'à présent, la cadence de développement et de publication de Python
était de dix-huit mois.

La 602e proposition d’amélioration de Python PEP (Python Enhancement Proposals)
de la Python Software Foundation (PSF) demandant à l'équipe de développement
de Python d’adopter un cycle de publication annuel a été acceptée.

Cette fréquence signifie moins de fonctionnalités par version, mais aussi
un feedback plus rapide sur les tests de fonctionnalités, moins de
changements potentiellement disruptifs pour chaque version, et donc plus
d'incitation pour les utilisateurs et les responsables de la distribution
Linux à mettre à jour Python plus souvent.

Cela signifie également que les nouvelles fonctionnalités proposées à la
fin du cycle de développement ne prendront pas autant de temps pour être
intégrées dans une nouvelle version.
Selon ce nouveau calendrier, Python 3.9 a été livré en octobre 2020.

La prochaine version Python 3.10, dont le développement pré-alpha a
officiellement démarré le 19 mai 2020, est entrée dans la phase de
développement alpha à la sortie de Python 3.9, et sa date de livraison
est fixée à octobre 2021.

Les prochaines versions de Python suivront le même calendrier.

Python devient plus rapide par défaut
==========================================

.. seealso::

   - :ref:`giacomelli_vector_call`

Chaque révision de Python bénéficie d'améliorations de performances par
rapport à la version précédente.

Python 3.9 apporte deux grandes améliorations qui augmentent les
performances sans nécessiter de modifications dans le code existant.

La première amélioration implique une utilisation plus large du protocole
**vectorcall** introduit dans Python 3.8.

En minimisant ou en supprimant les objets temporaires créés pour l'appel,
vectorcall accélère de nombreux appels de fonctions communes.

Dans Python 3.9, plusieurs éléments intégrés à Python - range, tuple,
set, frozenset, list, dict - utilisent vectorcall en interne pour
accélérer l'exécution.

La seconde amélioration des performances résulte d’une analyse plus
efficace du code source de Python.

Le nouvel analyseur pour le runtime CPython n'a pas été conçu pour
résoudre les problèmes de performance, mais plutôt pour traiter les
incohérences internes de l'analyseur original.

Cependant, ce nouvel analyseur apporte un autre avantage important : il
accélère l'analyse, en particulier **celle des gros volumes de code**.

Liens parser
--------------

.. seealso::

   - :ref:`gahjelle_new_python_parser`
   - :ref:`giacomelli_new_python_parser`

Plus de chaînes de caractères et de fonctions de dictionnaire
===============================================================

Python permet de manipuler facilement les types de données les plus
courants, et dans Python 3.9, cette facilité est étendue grâce à l’ajout
de nouvelles fonctionnalités pour les chaînes de caractères et les
dictionnaires.

Chaines de caractères
------------------------

.. seealso::

   - :ref:`gahjelle_string_prefix_suffix`
   - :ref:`giacomelli_string_prefix_suffix`

Concernant les chaînes de caractères, celles-ci s’enrichissent de
nouvelles méthodes pour supprimer les préfixes et les suffixes, opérations
qui ont longtemps nécessité beaucoup de :ref:`travail manuel <gahjelle_string_prefix_suffix>`.


Dictionnaires
-----------------

Quant aux dictionnaires, Python inclut désormais des opérateurs d'union,
un pour fusionner deux dictionnaires dans un nouveau dictionnaire et
un pour mettre à jour le contenu d'un dictionnaire avec un autre dictionnaire.



Les décorateurs perdent certaines restrictions
================================================

Les décorateurs permettent d'envelopper les fonctions Python pour modifier
leurs comportements de manière programmatique.

Auparavant, les décorateurs ne pouvaient être constitués que du symbole
@, d'un nom (par exemple func) ou d'un nom pointé (func.method) et,
éventuellement, d'un appel unique (func.method(arg1, arg2)).

Avec Python 3.9, les décorateurs peuvent désormais être constitués de
n'importe quelle expression valide.

Pendant longtemps, pour contourner cette restriction, les développeurs
devaient créer une fonction ou une expression lambda pour remplacer une
expression plus complexe quand elle était utilisée comme décorateur.
Désormais, n'importe quelle expression fera l'affaire, à condition
qu'elle produise quelque chose qui puisse servir de décorateur.

Nouvelles opérations de type
===============================

list + dict
-------------

.. seealso::

   - :ref:`gahjelle_list_dict`


Au cours des dernières versions, Python a étendu le support de l’indication
de type ou type hinting, principalement dans l'intérêt des linters et
des vérificateurs de code.

Les types n’étant pas imposés à l'exécution dans CPython, il n'est pas
prévu de faire de Python un langage statiquement typé.

Mais le type hinting est un outil puissant pour assurer la cohérence
dans les grandes bases de code, de sorte que le code Python peut toujours
bénéficier de l’indication de type.

Deux nouvelles fonctionnalités d’indication de type et d’annotations
de type ont été ajoutées à Python 3.9.

D'une part, les indications de type pour le contenu des collections
notamment, les **listes et les dictionnaires** sont maintenant disponibles
nativement en Python.

Cela signifie que l’on peut par exemple décrire une liste comme
**list[int]** (une liste d'entiers) sans avoir besoin de la bibliothèque
de type.

Annotations
-------------

.. seealso::

   - :ref:`gahjelle_annotated_type_hints`

Le deuxième ajout aux mécanismes de typage de Python concerne une
fonction flexible et des annotations de variable.

Il permet d'utiliser le type **Annotated** pour décrire un type en utilisant
des **métadonnées** qui peuvent être examinées à l'avance (avec des outils
de linting) ou à l'exécution.

Par exemple, on pourrait utiliser **Annotated [int, ctype("char")]** pour
décrire un entier qui devrait être considéré comme un type char en C.

Par défaut, Python ne ferait rien avec une telle annotation, mais elle
pourrait être utilisée par des linters de code.

Améliorations des classes internes
====================================

Le nettoyage, l’amélioration et la modernisation des classes internes
de Python font partie des travaux engagés par les développeurs de Python.

Mais Python 3.9 offre déjà quelques changements dans ces domaines.

Le premier concerne une refonte dans la manière dont les modules
interagissent avec le mécanisme d'importation.

Les modules d'extension Python, écrits en C, peuvent désormais utiliser
un nouveau mécanisme de chargement qui les fait se comporter davantage
comme des modules Python ordinaires lorsqu'ils sont importés.

Plusieurs modules de la bibliothèque standard de Python supportent
désormais ce comportement : _abc, audioop, _bz2, _codecs, _contextvars,
_crypt, _functools, _json, _locale, operator, resource, time, _weakref.

Le nouveau mécanisme de chargement permet non seulement de gérer les
modules d'extension de manière plus souple en Python, mais aussi de
nouvelles fonctionnalités comme des comportements d'accrochage avancés.

La seconde initiative de nettoyage porte sur l’interface interne stable
pour CPython, qui est garantie pour toute la durée de vie de Python 3.

Historiquement, chaque révision majeure de Python a été incompatible
avec les versions précédentes de l'ABI, ce qui imposait une recompilation
des modules d'extension pour chaque nouvelle version.

Désormais, tous les modules d'extension qui utilisent l'API stable
fonctionneront avec toutes les versions de Python.

Avec Python 3.9, les modules suivants de la bibliothèque standard
utilisent l'API stable : audioop, ast, grp, _hashlib, pwd, _posixsubprocess,
random, select, struct, termios, zlib.

Autres changements apportés à Python 3.9
=============================================

- La bibliothèque standard de Python supporte désormais la base de
  données des fuseaux horaires IANA Time Zone Database.
  Cette base est bien maintenue et largement utilisée, et le fait de
  pouvoir l’utiliser directement dans la bibliothèque date-heure de
  Python permettra de gagner beaucoup de temps.

- De nouvelles méthodes de chaînes de caractères permettent de supprimer
  facilement les préfixes et suffixes.
  C’est le genre d’usage commun qui demandait un peu trop de travail.
  Les nouvelles méthodes .removeprefix() et .removesuffix() renvoient
  une copie modifiée d'une chaîne sans le préfixe ou le suffixe en question,
  à condition qu'ils soient déjà présents dans la chaîne.
