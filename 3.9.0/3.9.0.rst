.. index::
   pair: Python ; 3.9.0 (2020-10-05 => TBD)
   pair: Version ; 3.9.0 (2020-10-05 => TBD)
   ! 3.9.0 (Expected 2020-10-05 => TBD)

.. _python_3_9_0:

=======================================================
**Python 3.9.0** (2020-10-05 => TBD)
=======================================================

.. seealso::

   - https://docs.python.org/3.9/whatsnew/3.9.html
   - https://github.com/python/cpython/releases/tag/v3.9.0
   - https://docs.python.org/3.9/whatsnew/changelog.html
   - https://www.python.org/dev/peps/pep-0596/#schedule
   - https://docs.python.org/3.9/whatsnew/changelog.html#changelog
   - https://discuss.python.org/tag/release
   - https://x.com/llanga/status/1313207780954828808?s=20
   - https://discuss.python.org/t/python-3-9-0-is-now-available-and-you-can-already-test-3-10-0a1/5375

.. figure:: announce_3_9_0.png
   :align: center

   https://x.com/llanga/status/1313207780954828808?s=20

.. figure:: python_rc2.jpeg
   :align: center

   https://x.com/llanga/status/1306535944451915776?s=20
   https://docs.python.org/release/3.9.0rc2/whatsnew/changelog.html#python-3-9-0-release-candidate-2


.. figure:: guido_3_9_rc1.png
   :align: center

   https://x.com/gvanrossum/status/1293323947690225666?s=20

.. figure:: guido_3_9_b1.png
   :align: center

   https://x.com/gvanrossum/status/1262766323626504192?s=20
   Python 3.9b1 is out.
   Try it! Thanks to everyone who contributed, to @baybryj , @llanga , @zooba
   (the release team), and to @VictorStinner for so many things


.. figure:: schedule.png
   :align: center

   https://www.python.org/dev/peps/pep-0596/#schedule

.. toctree::
   :maxdepth: 4

   announces/announces
   pep_0596/pep_0596
   pep_0584/pep_0584
   pep_0585/pep_0585
   pep_0590/pep_0590
   pep_0593/pep_0593
   pep_0615/pep_0615
   math_functions/math_functions
   pep_0616/pep_0616
   pep_0617/pep_0617
   pep_0489/pep_0489
   async/async
   documentation/documentation
   futures/futures
   graphlib/graphlib
   http_codes/http_codes
   ipv6/ipv6
   random/random
   conclusion/conclusion
