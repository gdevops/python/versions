.. index::
   pair: Lib; graphlib
   ! TopologicalSorter


.. _python39_topological_ordering:

==============================================================================
**new graphlib.TopologicalSorter class** Topological Ordering
==============================================================================

.. seealso::

   - https://github.com/python/cpython/blob/3.9/Lib/graphlib.py
   - https://docs.python.org/3.9/library/graphlib.html?highlight=graphlib#graphlib.TopologicalSorter




Lib/graphlib.py
===================

.. seealso::

   - https://github.com/python/cpython/blob/3.9/Lib/graphlib.py

.. code-block:: python
   :linenos:

    __all__ = ["TopologicalSorter", "CycleError"]

    _NODE_OUT = -1
    _NODE_DONE = -2


    class _NodeInfo:
        __slots__ = "node", "npredecessors", "successors"

        def __init__(self, node):
            # The node this class is augmenting.
            self.node = node

            # Number of predecessors, generally >= 0. When this value falls to 0,
            # and is returned by get_ready(), this is set to _NODE_OUT and when the
            # node is marked done by a call to done(), set to _NODE_DONE.
            self.npredecessors = 0

            # List of successor nodes. The list can contain duplicated elements as
            # long as they're all reflected in the successor's npredecessors attribute).
            self.successors = []


    class CycleError(ValueError):
        """Subclass of ValueError raised by TopologicalSorterif cycles exist in the graph

        If multiple cycles exist, only one undefined choice among them will be reported
        and included in the exception. The detected cycle can be accessed via the second
        element in the *args* attribute of the exception instance and consists in a list
        of nodes, such that each node is, in the graph, an immediate predecessor of the
        next node in the list. In the reported list, the first and the last node will be
        the same, to make it clear that it is cyclic.
        """

        pass


    class TopologicalSorter:
        """Provides functionality to topologically sort a graph of hashable nodes"""

        def __init__(self, graph=None):
            self._node2info = {}
            self._ready_nodes = None
            self._npassedout = 0
            self._nfinished = 0

            if graph is not None:
                for node, predecessors in graph.items():
                    self.add(node, *predecessors)

        def _get_nodeinfo(self, node):
            if (result := self._node2info.get(node)) is None:
                self._node2info[node] = result = _NodeInfo(node)
            return result

        def add(self, node, *predecessors):
            """Add a new node and its predecessors to the graph.

            Both the *node* and all elements in *predecessors* must be hashable.

            If called multiple times with the same node argument, the set of dependencies
            will be the union of all dependencies passed in.

            It is possible to add a node with no dependencies (*predecessors* is not provided)
            as well as provide a dependency twice. If a node that has not been provided before
            is included among *predecessors* it will be automatically added to the graph with
            no predecessors of its own.

            Raises ValueError if called after "prepare".
            """
            if self._ready_nodes is not None:
                raise ValueError("Nodes cannot be added after a call to prepare()")

            # Create the node -> predecessor edges
            nodeinfo = self._get_nodeinfo(node)
            nodeinfo.npredecessors += len(predecessors)

            # Create the predecessor -> node edges
            for pred in predecessors:
                pred_info = self._get_nodeinfo(pred)
                pred_info.successors.append(node)

        def prepare(self):
            """Mark the graph as finished and check for cycles in the graph.

            If any cycle is detected, "CycleError" will be raised, but "get_ready" can
            still be used to obtain as many nodes as possible until cycles block more
            progress. After a call to this function, the graph cannot be modified and
            therefore no more nodes can be added using "add".
            """
            if self._ready_nodes is not None:
                raise ValueError("cannot prepare() more than once")

            self._ready_nodes = [
                i.node for i in self._node2info.values() if i.npredecessors == 0
            ]
            # ready_nodes is set before we look for cycles on purpose:
            # if the user wants to catch the CycleError, that's fine,
            # they can continue using the instance to grab as many
            # nodes as possible before cycles block more progress
            cycle = self._find_cycle()
            if cycle:
                raise CycleError(f"nodes are in a cycle", cycle)

        def get_ready(self):
            """Return a tuple of all the nodes that are ready.

            Initially it returns all nodes with no predecessors; once those are marked
            as processed by calling "done", further calls will return all new nodes that
            have all their predecessors already processed. Once no more progress can be made,
            empty tuples are returned.

            Raises ValueError if called without calling "prepare" previously.
            """
            if self._ready_nodes is None:
                raise ValueError("prepare() must be called first")

            # Get the nodes that are ready and mark them
            result = tuple(self._ready_nodes)
            n2i = self._node2info
            for node in result:
                n2i[node].npredecessors = _NODE_OUT

            # Clean the list of nodes that are ready and update
            # the counter of nodes that we have returned.
            self._ready_nodes.clear()
            self._npassedout += len(result)

            return result

        def is_active(self):
            """Return True if more progress can be made and ``False`` otherwise.

            Progress can be made if cycles do not block the resolution and either there
            are still nodes ready that haven't yet been returned by "get_ready" or the
            number of nodes marked "done" is less than the number that have been returned
            by "get_ready".

            Raises ValueError if called without calling "prepare" previously.
            """
            if self._ready_nodes is None:
                raise ValueError("prepare() must be called first")
            return self._nfinished < self._npassedout or bool(self._ready_nodes)

        def __bool__(self):
            return self.is_active()

        def done(self, *nodes):
            """Marks a set of nodes returned by "get_ready" as processed.

            This method unblocks any successor of each node in *nodes* for being returned
            in the future by a a call to "get_ready"

            Raises :exec:`ValueError` if any node in *nodes* has already been marked as
            processed by a previous call to this method, if a node was not added to the
            graph by using "add" or if called without calling "prepare" previously or if
            node has not yet been returned by "get_ready".
            """

            if self._ready_nodes is None:
                raise ValueError("prepare() must be called first")

            n2i = self._node2info

            for node in nodes:

                # Check if we know about this node (it was added previously using add()
                if (nodeinfo := n2i.get(node)) is None:
                    raise ValueError(f"node {node!r} was not added using add()")

                # If the node has not being returned (marked as ready) previously, inform the user.
                stat = nodeinfo.npredecessors
                if stat != _NODE_OUT:
                    if stat >= 0:
                        raise ValueError(
                            f"node {node!r} was not passed out (still not ready)"
                        )
                    elif stat == _NODE_DONE:
                        raise ValueError(f"node {node!r} was already marked done")
                    else:
                        assert False, f"node {node!r}: unknown status {stat}"

                # Mark the node as processed
                nodeinfo.npredecessors = _NODE_DONE

                # Go to all the successors and reduce the number of predecessors, collecting all the ones
                # that are ready to be returned in the next get_ready() call.
                for successor in nodeinfo.successors:
                    successor_info = n2i[successor]
                    successor_info.npredecessors -= 1
                    if successor_info.npredecessors == 0:
                        self._ready_nodes.append(successor)
                self._nfinished += 1

        def _find_cycle(self):
            n2i = self._node2info
            stack = []
            itstack = []
            seen = set()
            node2stacki = {}

            for node in n2i:
                if node in seen:
                    continue

                while True:
                    if node in seen:
                        # If we have seen already the node and is in the
                        # current stack we have found a cycle.
                        if node in node2stacki:
                            return stack[node2stacki[node] :] + [node]
                        # else go on to get next successor
                    else:
                        seen.add(node)
                        itstack.append(iter(n2i[node].successors).__next__)
                        node2stacki[node] = len(stack)
                        stack.append(node)

                    # Backtrack to the topmost stack entry with
                    # at least another successor.
                    while stack:
                        try:
                            node = itstack[-1]()
                            break
                        except StopIteration:
                            del node2stacki[stack.pop()]
                            itstack.pop()
                    else:
                        break
            return None

        def static_order(self):
            """Returns an iterable of nodes in a topological order.

            The particular order that is returned may depend on the specific
            order in which the items were inserted in the graph.

            Using this method does not require to call "prepare" or "done". If any
            cycle is detected, :exc:`CycleError` will be raised.
            """
            self.prepare()
            while self.is_active():
                node_group = self.get_ready()
                yield from node_group
                self.done(*node_group)




Python 3.9 graphlib review
==================================

.. seealso::

   - https://paddy3118.blogspot.com/2020/08/python-39-graphlib-review.html


I had a look at the **TopologicalSorter class** and it seems to mix the
graph algorithm in with a use case in a less than ideal way.

- If given a graph of dependencies it will give one ordering that can satisfy the dependencies via its static_order() method only.
- It supports one way to add parallelism.

I had written a Topological Sort task on Rosetta Code, a Python
implementation, and had been both implementing and using T-sorts to
compile Electronics Design Automation libraries for over a decade,
(maybe 20+ years).

Below, I make slight alterations to the RC toposort2 function to create
toposort3, then compare it to the **new graphlib.TopologicalSorter class**.


The Modules A, B, C, D example
----------------------------------

They give the following input graph for T-sorting:

graph = {"D": {"B", "C"},
         "C": {"A"},
         "B": {"A"}}

Or more colloquially: D depends on, (or must come after),  both B and C; C
depends on A and B depends on A.

The static_order ordering given is::

    ('A', 'C', 'B', 'D')

Now those dependencies have only a "looser" ordering of the set of nodes
B and C. Nodes B and C can be swapped and it will still satisfy the
dependencies. B and C can be implemented in parallel in fact.


My routine, instead of returning a iterator over individual items, instead
iterates over sets of items.

- All items in each set must be completed/ordered before all those of a
  preceding set from the iterator.
- The items in each set may be ordered/completed in any way or completed in parallel.

My routines result iterator if turned into a tuple would yield::

    ({'A'}, {'B', 'C'}, {'D'})

If this were a calculated compilation order for Verilog and VHDL libraries
then it clearly shows the opportunity for parallelism in compiling B and C
and that a maximum of two libraries can be compiled in parallel.


RosettaCode task Example
-----------------------------

The RosettaCode task graph shows even  more scope for parallelism.

Here is the task data run through the new module and my toposort function:

.. code-block:: python
   :linenos:

    from functools import reduce
    from pprint import pprint as pp
    from collections import defaultdict

    from graphlib import TopologicalSorter

    # LIBRARY     mapped_to     LIBRARY DEPENDENCIES
    data = {
        'des_system_lib':   set('std synopsys std_cell_lib des_system_lib dw02 dw01 ramlib ieee'.split()),
        'dw01':             set('ieee dw01 dware gtech'.split()),
        'dw02':             set('ieee dw02 dware'.split()),
        'dw03':             set('std synopsys dware dw03 dw02 dw01 ieee gtech'.split()),
        'dw04':             set('dw04 ieee dw01 dware gtech'.split()),
        'dw05':             set('dw05 ieee dware'.split()),
        'dw06':             set('dw06 ieee dware'.split()),
        'dw07':             set('ieee dware'.split()),
        'dware':            set('ieee dware'.split()),
        'gtech':            set('ieee gtech'.split()),
        'ramlib':           set('std ieee'.split()),
        'std_cell_lib':     set('ieee std_cell_lib'.split()),
        'synopsys':         set(),
        }
    for k, v in data.items():
        v.discard(k)   # Ignore self dependencies


    # LIBRARY     mapped_to     LIBRARY PREDECESSORS
    data_invert = defaultdict(set)
    for lib, deps in data.items():
        for dep in deps:
            data_invert[dep].add(lib)


    ts = TopologicalSorter(data)
    graphlib_order = tuple(ts.static_order())

    def toposort3(data):
        extra_items_in_deps = reduce(set.union, data.values()) - set(data.keys())
        data.update({item:set() for item in extra_items_in_deps})
        while True:
            ordered = set(item for item,dep in data.items() if not dep)
            if not ordered:
                break
            yield ordered
            data = {item: (dep - ordered) for item,dep in data.items()
                    if item not in ordered}
        assert not data, "A cyclic dependency exists amongst %r" % data

    paddys_order = tuple(toposort3(data))

    print('Python 3.9 graphlib gives a topological ordering:')
    pp(graphlib_order)

    print('\nMy topological ordering of sets that must be in tuple order, but each item in an individual set can be executed in any order for that set, or in *parallel*:')
    pp(paddys_order)


Output::

    python 3.9 graphlib gives a topological ordering:
    ('des_system_lib',
     'dw03',
     'dw07',
     'dw06',
     'dw04',
     'dw05',
     'ramlib',
     'std_cell_lib',
     'synopsys',
     'dw02',
     'dw01',
     'std',
     'dware',
     'gtech',
     'ieee')

    My topological ordering of sets that must be in tuple order, but each item in an individual set can be executed in any order for that set, or in *parallel*:
    ({'synopsys', 'std', 'ieee'},
     {'dware', 'ramlib', 'gtech', 'std_cell_lib'},
     {'dw07', 'dw02', 'dw06', 'dw01', 'dw05'},
     {'des_system_lib', 'dw04', 'dw03'})

From the output above, my routine doesn't hide what can be run in parallel,
and encompasses every possible ordering of items, wheras the graphlib
module only shows one ordering, and if the graphlib modules parallelism
support is used, it is more difficult to get an idea of the opportunities
for parallelism rather than just starting up another task.

I can see that I can use a maximum of five and a minimum of 3 tasks in
parallel and that those parallel tasks can be executed in a minimum of
four sequential steps.


So, That's why and how  I think graphlib could be better.
