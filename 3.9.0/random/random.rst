
.. _python_3_9_random:

==============================================================================
Python 3.9 Feature **Random Bytes Generation**
==============================================================================




Feature: Random Bytes Generation
=====================================

.. seealso::

   - https://towardsdatascience.com/10-awesome-python-3-9-features-b8c27f5eba5c
   - https://bugs.python.org/issue40286

Another feature that has been added in the 3.9 release is the function
**random.Random.randbytes()**.

This function can be used to generate random bytes.

We can generate random numbers but what if we needed to generate random
bytes? Prior to 3.9 version, the developers had to get creative to generate
the random bytes.

Although we can use os.getrandom(), os.urandom() or secrets.token_bytes()
but we can’t generate pseudo-random patterns.

As an instance, to ensure the random numbers are generated with expected
behaviour and the process is reproducible, we generally use the seed
with random.Random module.

As a result, random.Random.randbytes() method has been introduced.
It can generate random bytes in a controlled manner too.
