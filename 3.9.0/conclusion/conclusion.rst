
.. _python39_conclusion:

==============================================================================
Conclusion
==============================================================================



Articles
=========

https://martinheinz.dev
--------------------------

Probably not all of these changes are relevant to your daily programming,
but I think it's good to be at least aware of the first 2 additions
(| operator and TopologicalSorter) as they might come in handy at some point.

That said Python 3.9 is still in alpha phase, so there still might be
some additional changes up until 18.5.2020 (first beta release).

But even then you should not use this version, as it is not stable nor
production ready (not at least until October).
