
.. _python_39_doc:

==============================================================================
Documentation
==============================================================================




I contributed to **Python 3.9 typing** docs by Luciano Ramalho
=================================================================

.. seealso::

   - https://x.com/ramalhoorg/status/1313189602082066433?s=20
   - https://docs.python.org/3.9/library/typing.html#module-contents

I contributed to **Python 3.9 typing docs**: created sections under "Module Contents",
reorganized 60+ entries that were there and added lots of usage warnings
about PEP 585.

:ref:`PEP 544 <pep_0544>` and :ref:`585 <pep_0585>` are great improvements
to the usability of type hints.
