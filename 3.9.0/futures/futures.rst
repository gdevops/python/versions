
.. _python_3_9_futures:

==============================================================================
Python 3.9 **concurrent.futures news**
==============================================================================




Ability To **Cancel Concurrent Futures**
===========================================

.. seealso::

   - https://towardsdatascience.com/10-awesome-python-3-9-features-b8c27f5eba5c
   - https://bugs.python.org/issue30966

A new parameter **cancel_futures** have been added to the concurrent.futures.Executor.shutdown().

This parameter cancels all of the pending futures that have not started.

Prior to version 3.9, the process would wait for them to complete before
shutting down the executor.


Explanation
----------------

The new parameter cancel_futures have been added to both ThreadPoolExecutor
and ProcessPoolExecutor.

The way it works is when the value of the parameter is True then all
pending futures would be canceled when the shutdown() function is called.

In a nutshell, when the shutdown() is executed, the interpreter checks
if the executor is not garbage collected.

If it is still in memory then it gets all of the pending worker items
and then cancels the futures.

Once there are no pending work items then it shuts down the worker.
