
.. _python39_builtin_generic_types:
.. _pep_0585:

==============================================================================
**PEP 585** Builtin Generic Types (typing)
==============================================================================

.. seealso::

   - https://www.python.org/dev/peps/pep-0585/
   - https://docs.python.org/3.9/whatsnew/3.9.html#pep-585-builtin-generic-types




Description
=============

In type annotations you can now use built-in collection types such as
list and dict as generic types instead of importing the corresponding
capitalized types (e.g. List or Dict) from **typing**.

Some other types in the standard library are also now generic, for
example queue.Queue.

Example:

.. code-block:: python


    def greet_all(names: list[str]) -> None:
        for name in names:
            print("Hello", name)
