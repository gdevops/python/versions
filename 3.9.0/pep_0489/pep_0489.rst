
.. _python39_pep_489__3_9:

====================================================================================================================================
**_abc, audioop, ... _functools, _json, operator, resource,...**  now use multiphase initialization as defined by **PEP 489**
====================================================================================================================================

.. seealso::

   - https://www.python.org/dev/peps/pep-0489/



Description
=============

A number of Python modules (_abc, audioop, _bz2, _codecs, _contextvars,
_crypt, _functools, _json, _locale, operator, resource, time, _weakref)
now use multiphase initialization as defined by PEP 489 ;
