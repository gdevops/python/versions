
.. _python39_http_codes:

==============================================================================
HTTP codes
==============================================================================

.. seealso::

   - https://docs.python.org/3.9/library/http.html#http.HTTPStatus




Articles
=========

https://martinheinz.dev
--------------------------

http.HTTPStatus
++++++++++++++++++


Last but not least, well actually... are HTTP status codes added to
**http.HTTPStatus**. Namely those are:

.. code-block:: python

    import http

    http.HTTPStatus.EARLY_HINTS
    # <HTTPStatus.EARLY_HINTS: 103>

    http.HTTPStatus.TOO_EARLY
    # <HTTPStatus.TOO_EARLY: 425>

    http.HTTPStatus.IM_A_TEAPOT
    # <HTTPStatus.IM_A_TEAPOT: 418>

Looking at these status code, I can't quite see why would you ever use
them.

That said, it's great to finally have I'm a Teapot status code at our
disposal.

It's great quality of life improvement that I can now use
http.HTTPStatus.IM_A_TEAPOT when returning this code from production
server (sarcasm, Please never do that...).
