.. index::
   pair: Video ; 3.10.0 (2021-10-04)

.. _video_python_3_10_0:

=====================================================
**Release video for Python 3.10.0** (2021-10-04)
=====================================================


.. figure:: images/brandt_bucher_fast_python.png
   :align: center

.. figure:: images/discord_mailing_liste.png
   :align: center

.. figure:: images/erreurs.png
   :align: center

.. figure:: images/erreurs_maintenance.png
   :align: center

.. figure:: images/f_string_parser_3_11.png
   :align: center

.. figure:: images/f_string_parser_3_11_bis.png
   :align: center


.. figure:: images/garbage_collector.png
   :align: center

.. figure:: images/get_annotations.png
   :align: center

.. figure:: images/type_alias.png
   :align: center

.. figure:: images/pattern_matching_3_11.png
   :align: center


.. figure:: images/pep_0654.png
   :align: center

.. figure:: images/sentinel_draft_11.png
   :align: center
