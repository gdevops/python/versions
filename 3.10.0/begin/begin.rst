
.. _python_3_10_0_begin:

=====================================================
**Python 3.10.0** begin
=====================================================

.. seealso::

   - https://docs.python.org/3.10/whatsnew/3.10.html
   - https://x.com/pyblogsal



Begin
======

.. seealso::

   - https://mail.python.org/archives/list/python-committers@python.org/thread/44TLJO5YX6XYM4ICWSHMBMCKPBBQQP5S/

.. figure:: ../pablo_galindo_3_10.png
   :align: center

   https://x.com/llanga/status/1262883403537448961?s=20


Now that #Python 3.9's feature set is frozen, work has begun on
**Python 3.10**.

And with it, a new Release Manager: **Pablo Galindo Salgado** https://x.com/pyblogsal !

He already started strong, making sure Python is ready for a two-digit
minor version.

But that's his story to tell! Welcome! 🤜🏻 🤛🏻
