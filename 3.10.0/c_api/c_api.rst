.. index::
   C-API; Python 3.10

.. _python_3_10_0_c_api:

=====================================================
**Python 3.10 C-API**
=====================================================

- https://vstinner.github.io/c-api-python3_10-changes.html
- https://github.com/hpyproject/hpy/wiki/c-api-next-level-manifesto
- https://github.com/vstinner/misc/blob/main/cpython/pep-c-api-next-level.rst

PEP 652 -- Maintaining the Stable ABI
=========================================

https://www.python.org/dev/peps/pep-0652/

C API changes between Python 3.5 to 3.10 by Victor Stinner
===========================================================

- https://vstinner.github.io/c-api-python3_10-changes.html
- https://x.com/VictorStinner/status/1445064551985303558?s=20

I'm trying to enhance and to fix the Python C API for 5 years.

My first goal was to shrink the C API without breaking third party C
extensions.
I hid many private functions from the public functions: I moved them to
the "internal C API". I also deprecated and removed many functions.

Between Python 3.5 and 3.10, 80 symbols have been removed.
Python 3.10 is the first Python version exporting less symbols than its
previous version!

Since Python 3.8, the C API is organized as 3 parts:

- Include/ directory: Limited API
- Include/cpython/ directory: CPython implementation details
- Include/internal/ directory: The internal API

The devguide Changing Python’s C API documentation now gives guidelines
for C API additions, like avoiding borrowed references.

The limited C API got a few more functions, whereas broken and private
functions have been removed.
The Stable ABI is now explicitly defined and documented in the C API
Stability page.

This article lists all C API changes, not only the ones done by me.


Python 3.10
--------------

Move 8 header files from Include/ to Include/cpython/:

- odictobject.h
- parser_interface.h
- picklebufobject.h
- pyarena.h
- pyctype.h
- pydebug.h
- pyfpe.h
- pytime.h

Python 3.10 added a Include/README.rst documentation to explain this
organization and give guidelines for adding new functions.

For example, new functions in the public C API must not steal references
nor return borrowed references. In the meanwhile, this documentation moved
to the devguide: Changing Python’s C API.
