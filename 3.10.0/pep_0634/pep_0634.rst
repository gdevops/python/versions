.. index::
   pair: PEP ; 0634

.. _pep_0634:

=============================================================================================
**PEP-0634** **Structural Pattern Matching**
=============================================================================================

.. seealso::

   - https://www.python.org/dev/peps/pep-0634/
   - https://x.com/fperez_org/status/1278401194088517632?s=20




.. figure:: fenando_perez.png
   :align: center



Examples
=========


Example class Point
---------------------

.. seealso::

   - https://github.com/gvanrossum/patma/pull/117#issuecomment-652127366
   - https://mail.python.org/archives/list/python-dev@python.org/thread/47YR2LUTLWA4SGDHE66AXERVHW5EDK6I/


.. code-block:: python
   :linenos:

    from dataclasses import dataclass

    @dataclass
    class Point:
        x: int
        y: int

    def whereis(point):
        match point:
            case Point(0, 0):
                print("Origin")
            case Point(0, y):
                print(f"Y={y}")
            case Point(x, 0):
                print(f"X={x}")
            case Point():
                print("Somewhere else")
            case _:
                print("Not a point")


    def whereis(point):
        match point:
            case (0, 0):
                print("Origin")
            case (0, y):
                print(f"Y={y}")
            case (x, 0):
                print(f"X={x}")
            case (_, _):
                print("Somewhere else")
            case _:
                print("Not a point")
