.. index::
   pair: Type ; annotation

.. _python_3_10_0_type_annotation:

=====================================================
**Type annotation improvements**
=====================================================


Type annotation improvements
================================

- https://treyhunner.com/2021/10/whats-great-about-python-3-dot-10/

If you use type annotations, type unions are even easier now using the | operator
(in addition to typing.Union).

Other big additions in type annotation land include parameter specification
variables, type aliases, and user-defined type guards.

I still don’t use type annotations often, but **these features are a pretty
big deal for Python devs who do**.

Also if you’re introspecting annotations, calling the inspect.get_annotations function
is recommended over accessing __annotations__ directly or calling the
typing.get_type_hints function.
