.. index::
   pair: black; parser

.. _python_3_10_0_black:

==========================================================
**Python 3.10.0** and black => **we need a new parser**
==========================================================

- https://github.com/psf/black/issues/2318
- https://bugs.python.org/issue36541
- https://bugs.python.org/issue40360
- https://docs.google.com/document/d/1QPqm01E7MIRi_l4jrgCGOFVThP_HINnkm4hmVZC_CSg/edit#heading=h.qokppq9m1uzy
- https://github.com/Instagram/LibCST
- https://github.com/davidhalter/parso

https://github.com/psf/black/issues/2318
==============================================

https://github.com/psf/black/issues/2318#issue-915481022
------------------------------------------------------------

- https://github.com/psf/black/issues/2318#issue-915481022

Currently, Black uses a vendored version of lib2to3 for parsing.
This works well for parsing Python 2 and early Python 3, but Python has
now moved on to a **PEG-based parser** (`PEP 617 <https://www.python.org/dev/peps/pep-0617/>`_), and lib2to3 is no longer
being maintained.

**So we need a new parser**.

There are a few existing options that we could leverage (`Parso <https://github.com/davidhalter/parso>`_, LibCST),
but **it's going to be a lot of work to do the migration**.

WE're doing some early brainstorming in a `Google doc <https://docs.google.com/document/d/1QPqm01E7MIRi_l4jrgCGOFVThP_HINnkm4hmVZC_CSg/edit#heading=h.qokppq9m1uzy>`_.
This issue exists so that we have a public record that we know this is
a problem.

Concrete pieces of syntax that are blocked by this new grammar include
**parenthesized context managers and the match statement in Python 3.10**.


https://github.com/psf/black/issues/2318#issuecomment-874209546
------------------------------------------------------------------

- https://github.com/psf/black/issues/2318#issuecomment-874209546

The main bug is: https://bugs.python.org/issue40360 (and also
https://bugs.python.org/issue36541).

I think that there's a fairly straightforward way of wrapping the new
Python parser to give the necessary functionality that "Black" (and other
source-level tools) need.

However, it's a non-trivial amount of work, and I'm loathe to do it unless
I'm sure it'll be used and that nobody else is doing the work.
(There appears to be one existing wrapper, namely leoAst.py;

I've looked at it a bit but it seems much more complicated than necessary
and therefore could be both difficult to use and a maintenance issue.)

Some other discussion at kamahen/pykythe#27 google/yapf#825 (comment) ,
google/yapf#894 (comment) and elsewhere.
