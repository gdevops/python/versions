
.. _python_3_10_0_zip_length:

=====================================================
Length-checking for the zip function
=====================================================

- https://docs.python.org/3.10/library/functions.html#zip

Length-checking for the zip function
=======================================

There’s a Python Morsels exercise called strict_zip.

It’s now become a “re-implement this already built-in functionality”
exercise. Still useful for the sake of learning how zip is implemented,
but no longer useful day-to-day code.

Why isn’t it useful ? Because zip now accepts a strict argument !
So if you’re working with iterables that might be different lengths but
shouldn’t be, passing strict=True is now recommended when using zip.
