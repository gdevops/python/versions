.. index::
   pair: Python ; 3.10.0 (2021-10-04)

.. _python_3_10_0:

=====================================================
**Python 3.10.0** (2021-10-04)
=====================================================

- https://github.com/python/cpython/releases/tag/v3.10.0
- https://www.python.org/dev/peps/pep-0619/
- https://www.python.org/dev/peps/pep-0619/#schedule
- https://docs.python.org/3.10/whatsnew/3.10.html
- https://docs.python.org/3.10/whatsnew/changelog.html#changelog
- https://x.com/pyblogsal
- https://fr.wikipedia.org/wiki/Python_%28langage%29#Historique_des_versions
- https://en.wikipedia.org/wiki/History_of_Python#Table_of_versions
- https://www.youtube.com/watch?v=AHT2l3hcIJg (Python 3.10 Release Stream — with Pablo Galindo)
- https://x.com/pyblogsal/status/1445120766350184450?s=20
- https://mail.python.org/archives/list/python-dev@python.org/thread/OQWNWZWDPASOUOAT6VPUXIXBH2THYREC/
- https://www.youtube.com/watch?v=JteTO3EE7y0 (What's New in Python 3.10: featuring Brandt Bucher, Lukasz Llanga and Sebastian Ramirez)

.. figure:: logo_sortie_python_3_10.jpeg
   :align: center
   :width: 700


.. figure:: twitter_pablo_galindo_3_10.png
   :align: center
   :width: 700

   https://x.com/pyblogsal/status/1445120766350184450?s=20


.. figure:: videos/livraison/images/f_string_parser_3_11_bis.png
   :align: center

   https://www.youtube.com/watch?v=AHT2l3hcIJg


.. toctree::
   :maxdepth: 3

   begin/begin
   documentation/documentation
   error_messages/error_messages
   zip_length/zip_length
   match_statement/match_statement
   slots_dataclass/slots_dataclass
   types_annotations/types_annotations
   pep_0637/pep_0637
   pep_0636/pep_0636
   pep_0634/pep_0634
   pep_0604/pep_0604
   pep_0563/pep_0563
   pep_0554/pep_0554
   c_api/c_api
   pep_0619/pep_0619
   videos/videos
   real_python/real_python
   testdriven/testdriven
   black/black
   bugs/bugs
