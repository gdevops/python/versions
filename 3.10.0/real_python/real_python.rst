
.. _python_3_10_0_real_python:

=====================================================
**Python 3.10: Cool New Features for You to Try**
=====================================================

- https://realpython.com/python310-new-features/

.. figure:: cool_new_features.png
   :align: center


structural-pattern-matching
==============================

- https://realpython.com/python310-new-features/#structural-pattern-matching
