.. index::
   pair: Python ; 3.12.4 (2024-06-06)


.. _python_3_12_4:

=====================================================
**Python 3.12.4** (2024-06-06)
=====================================================


- https://github.com/python/cpython/releases/tag/v3.12.4
- https://blog.python.org/2024/06/python-3124-released.html

