
=============================================================================
A new type of interpreter based on tail calls has been added to CPython
=============================================================================

- https://docs.python.org/3.14/whatsnew/3.14.html#whatsnew314-tail-call

A new type of interpreter based on tail calls has been added to CPython. 

For certain newer compilers, this interpreter provides significantly better 
performance. 

Preliminary numbers on our machines suggest anywhere from -3% to 30% faster 
Python code, and a geometric mean of 9-15% faster on pyperformance depending 
on platform and architecture.

This interpreter currently only works with Clang 19 and newer on x86-64 
and AArch64 architectures. 

However, we expect that a future release of GCC will support this as well.

Our initial benchmarks show a ~20-30% performance improvement across CPython
================================================================================

- https://simonwillison.net/2025/Feb/13/python-3140a5/

This is an optimization that was first discussed in faster-cpython in January 2024, 
then landed earlier this month by Ken Jin and included in the 3.14a05 release. 

The alpha release notes say:

::

    A new type of interpreter based on tail calls has been added to CPython. 
    For certain newer compilers, this interpreter provides significantly 
    better performance. 
    
    Preliminary numbers on our machines suggest anywhere from -3% to 30% 
    faster Python code, and a geometric mean of 9-15% faster on pyperformance 
    depending on platform and architecture. 
    
    The baseline is Python 3.14 built with Clang 19 without this new interpreter.
    
    This interpreter currently only works with Clang 19 and newer on x86-64 
    and AArch64 architectures. 
    
    However, we expect that a future release of GCC will support this as well.
