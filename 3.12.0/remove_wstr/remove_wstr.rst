
.. _remove_wstr:

=====================================================
**PEP 623 – Remove wstr from Unicode**
=====================================================

- https://peps.python.org/pep-0623/


Motivation
===============

Memory usage
----------------

str is one of the most used types in Python. Even most simple ASCII
strings have a wstr member. It consumes 8 bytes per string on 64-bit systems.


Runtime overhead
------------------

To support legacy Unicode object, many Unicode APIs must call PyUnicode_READY().

We can remove this overhead too by dropping support of legacy Unicode object.
Simplicity

Supporting legacy Unicode object makes the Unicode implementation more
complex.

Until we drop legacy Unicode object, it is very hard to try other Unicode
implementation like UTF-8 based implementation in PyPy.
