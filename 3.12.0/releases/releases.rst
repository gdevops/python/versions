
.. _python_3_12_0_releases:

=====================================================
**Python 3.12.0 releases**
=====================================================

- https://peps.python.org/pep-0693/ (Python 3.12 Release Schedule)

Release Schedule
==================

Note: the dates below use a 17-month development period that results in
a 12-month release cadence between major versions, as defined by PEP 602.

Actual:


- 3.12 development begins: Sunday, 2022-05-08
- 3.12.0 alpha 1: Monday, 2022-10-24
- 3.12.0 alpha 2: Monday, 2022-11-14
- 3.12.0 alpha 3: Tuesday, 2022-12-06
- 3.12.0 alpha 4: Tuesday, 2023-01-10
- 3.12.0 alpha 5: Tuesday, 2023-02-07
- 3.12.0 alpha 6: Tuesday, 2023-03-07
- 3.12.0 alpha 7: Tuesday, 2023-04-04
- 3.12.0 beta 1: Monday, 2023-05-22 (No new features beyond this point.)
- 3.12.0 beta 2: Tuesday, 2023-06-06
- 3.12.0 beta 3: Monday, 2023-06-19
- 3.12.0 beta 4: Tuesday, 2023-07-11
- 3.12.0 candidate 1: Sunday, 2023-08-06


Expected:

- 3.12.0 candidate 2: Monday, 2023-09-04
- 3.12.0 final: Monday, 2023-10-02

Subsequent bugfix releases every two months.
